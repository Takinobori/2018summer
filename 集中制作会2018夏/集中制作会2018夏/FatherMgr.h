#pragma once
#include "Father.h"
#include "Player.h"
#include "Map.h"

struct FatherMgr
{
	tFather father;									// 父構造体
	void Initialize(tPoint p, Muki muki);			// 父の初期化、その他もあれば
	void Delete();									// 終了処理
	void Update(Map* map, tPlayer* player);			// 父のUpdateと違いマップやプレイヤーに当たった時の処理や周りに影響する処理を行う
	void Draw(Map* map);							// 描画、描画結果が見たくなったら少し弄るから聞いて
};
