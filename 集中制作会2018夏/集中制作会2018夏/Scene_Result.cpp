#include "Scene_Result.h"

void Scene_Result::Initialize() {}
void Scene_Result::Delete() {}
void Scene_Result::Update()
{
	// シーンの移動処理(デバック用)
	{
		if (Keyboard::Get(KEY_INPUT_SPACE) == 1)
		{
			Base_Scene::next_scene = eScene_Title;
		}
		if (Keyboard::Get(KEY_INPUT_BACK) == 1)
		{
			Base_Scene::next_scene = eScene_Game;
		}
	}
}
void Scene_Result::Draw()
{
	// デバック用文章
	{
		DrawFormatString(64, 64, 0xffffff, "リザルト画面です。");
		DrawFormatString(64, 64 * 2, 0xffffff, "バックスペースでゲーム画面に戻ります。");
		DrawFormatString(64, 64 * 3, 0xffffff, "スペースキーでタイトル画面に移行します。");
	}
}