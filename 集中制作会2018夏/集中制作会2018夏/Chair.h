#pragma once
#include "Object.h"
#include "Base_Character.h"

class Chair :public Object
{
private:
	Muki muki;

public:
	Chair(tPoint point, Muki _muki);
	~Chair();
	void Update();
	void Draw(int Add_x, int Add_y);
	bool Active(tPlayer* player);
	void Effect();
};

