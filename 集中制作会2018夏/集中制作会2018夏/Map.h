#pragma once
#include <DxLib.h>
#include <vector>
#include <array>
#include <string>
#include <fstream>
#include <sstream>
#include <bitset>
#include "Player.h"

enum MapMode
{
	eMap_Castle, eMap_Castle_D, eMap_Real, 
};
class Map
{
private:
	MapMode             mode;
	std::array<int, 2>  image;
	std::ifstream       ifs;
	std::ofstream       ofs;

	void Load_File(int Stage_num);
	void Load_Graph();
public:
	int x;
	int y;
	std::vector<std::vector<int>>data;
	Map(MapMode _mode, int Stage_num, tPlayer* player);
	~Map();
	void Reset(MapMode _mode, int Stage_num, tPlayer* player);
	void Draw();
	int GetSize_X();
	int GetSize_Y();
	int Get_x();
	int Get_y();
};