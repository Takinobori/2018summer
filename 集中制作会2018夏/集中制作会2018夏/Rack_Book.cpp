#include "Rack_Book.h"

Rack_Book::Rack_Book(tPoint point, int Text_num) :Object(point, tPoint::Get_Point(point.x, point.y - 16), true, (Text_num != 0), tSize::Get_Size(32, 64), tSize::Get_Size(64, 96))
{
	image = LoadGraph("image/Object/Rack/book.png");
	if (story_flag)
	{
		str = "Text/Object/Rack/Book/" + std::to_string(Text_num) + ".txt";
	}
}
Rack_Book::~Rack_Book()
{
	DeleteGraph(image);
}
void Rack_Book::Update()
{
}
void Rack_Book::Draw(int Add_x, int Add_y)
{
	DrawGraph(point.x + Add_x, point.y + Add_y, image, true);
}
void Rack_Book::Effect()
{
}
