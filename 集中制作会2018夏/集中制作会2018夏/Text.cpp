#include "Text.h"

Text_Data tText::data;
std::ifstream tText::ifs;

Text_Data tText::Get_Text(std::string pass)
{
	ifs.open(pass);

	std::string str_one;
	std::string str_w;
	int X = 0;
	int Y = 0;
	data.resize(0);
	while (getline(ifs, str_w))
	{
		X = 0;
		std::istringstream iss(str_w);
		data.resize(Y + 1);
		while (getline(iss, str_one, ','))
		{
			data[Y].push_back(str_one);
			X++;
		}
		Y++;
	}
	ifs.close();


	return data;
}
