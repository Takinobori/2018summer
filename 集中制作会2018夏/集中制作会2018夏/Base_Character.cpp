#include "Base_Character.h"

Base_Character Base_Character::Get_Base_Character(tPoint p, tSize s, tSize gr_s, Muki muki)
{
	Base_Character base_character;
	base_character.point = p;
	base_character.back_point = p;
	base_character.size = s;
	base_character.gr_size = gr_s;
	base_character.muki = muki;
	return base_character;
}
void Base_Character::Set_Back()
{
	back_point = point;
}
void Base_Character::Go_Back()
{
	point = back_point;
}
