#include "Map.h"

Map::Map(MapMode _mode, int Stage_num, tPlayer* player)
{
	mode = _mode;
	Load_Graph();
	Load_File(Stage_num);
	x = (1920 - (signed)data[0].size() * 64) / 2;
	y = (1080 - (signed)data.size() * 64) / 2;
	for (int i = 0, n = (signed)data.size(); i < n; i++)
	{
		for (int j = 0, o = (signed)data[i].size(); j < o; j++)
		{
			if (data[i][j] == 9)
			{
				player->base.point.x = j * PANEL;
				player->base.point.y = i * PANEL;
				data[i][j] = 1;
			}
		}
	}
}
Map::~Map()
{
	for (int i = 0, n = (signed)image.size(); i < n; i++)
	{
		DeleteGraph(image[i]);
	}
}
void Map::Load_File(int Stage_num)
{
	std::string pass;
	pass = "Map/" + std::to_string(Stage_num) + ".csv";
	ifs.open(pass.c_str());
	std::string str_one;
	std::string str_w;
	int X = 0;
	int Y = 0;
	if (!data.empty())
	{
		data[0].resize(0);
	}
	while (getline(ifs, str_w))
	{
		X = 0;
		std::istringstream iss(str_w);
		data.resize(Y + 1);
		while (getline(iss, str_one, ','))
		{
			data[Y].push_back(stoi(str_one));
			X++;
		}
		Y++;
	}
	ifs.close();
}
void Map::Load_Graph()
{
	std::string pass;
	switch (mode)
	{
	case eMap_Castle:
		image[0] = LoadGraph("image/マップチップ/c-tile.png");
		image[1] = LoadGraph("image/マップチップ/c-wall.png");
		break;
	case eMap_Castle_D:
		image[0] = LoadGraph("image/マップチップ/c-tile-d.png");
		image[1] = LoadGraph("image/マップチップ/c-wall-d.png");
		break;
	case eMap_Real:
		image[0] = LoadGraph("image/マップチップ/g-tile.png");
		image[1] = LoadGraph("image/マップチップ/g-wall.png");
		break;

	default:
		break;
	}
}
void Map::Reset(MapMode _mode, int Stage_num, tPlayer* player)
{
	mode = _mode;
	for (int i = 0, n = (signed)image.size(); i < n; i++)
	{
		DeleteGraph(image[i]);
	}
	Load_Graph();
	Load_File(Stage_num);
	x = (1920 - (signed)data[0].size() * 64) / 2;
	y = (1080 - (signed)data.size() * 64) / 2;
	for (int i = 0, n = (signed)data.size(); i < n; i++)
	{
		for (int j = 0, o = (signed)data[i].size(); j < o; j++)
		{
			if (data[i][j] == 9)
			{
				player->base.point.x = j * PANEL;
				player->base.point.y = i * PANEL;
				data[i][j] = 1;
			}
		}
	}
}
void Map::Draw()
{
	for (int i = 0, n = (signed)data.size(); i < n; i++)
	{
		for (int j = 0, o = (signed)data[i].size(); j < o; j++)
		{
			if (data[i][j] != 0)
			{
				DrawGraph(x + j * 64, y + i * 64, image[data[i][j] - 1], true);
			}
		}
	}
}
int Map::GetSize_X()
{
	return (signed)data[0].size();
}
int Map::GetSize_Y()
{
	return (signed)data.size();
}
int Map::Get_x()
{
	return x;
}
int Map::Get_y()
{
	return y;
}
