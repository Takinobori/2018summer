#include "Player.h"

void tPlayer::Initialize(tPoint p, Muki muki)
{
	base = Base_Character::Get_Base_Character(p, tSize::Get_Size(32, 48), tSize::Get_Size(64, 64), muki);
	std::string pass;
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			pass = "image/Character/Player/" + std::to_string(i) + "/" + std::to_string(j) + ".png";
			image[i][j] = LoadGraph(pass.c_str());
		}
	}
	gr_counter = 0;
	walk_flag = false;
}
void tPlayer::Delete() 
{
	for (int i = 0, n = (signed)image.size(); i < n; i++)
	{
		for (int j = 0, o = (signed)image[i].size(); j < o; j++)
		{
			DeleteGraph(image[i][j]);
		}
	}
}
void tPlayer::Update()
{
	base.Set_Back();
	walk_flag = true;
	if (Keyboard::Get(KEY_INPUT_UP) > 0)
	{
		base.point.y -= 3;
		base.muki = UP;
	}
	else if (Keyboard::Get(KEY_INPUT_LEFT) > 0)
	{
		base.point.x -= 3;
		base.muki = LEFT;
	}
	else if (Keyboard::Get(KEY_INPUT_DOWN) > 0)
	{
		base.point.y += 3;
		base.muki = DOWN;
	}
	else if (Keyboard::Get(KEY_INPUT_RIGHT) > 0)
	{
		base.point.x += 3;
		base.muki = RIGHT;
	}
	else
	{
		walk_flag = false;
	}
	hit_point = tPoint::Get_Point(base.point.x, base.point.y + 64);

	if (walk_flag)
	{
		gr_counter = (gr_counter + 1) % 30;
	}
	else
	{
		gr_counter = 30;
	}

}
void tPlayer::Draw(int add_x, int add_y)
{
	DrawGraph(add_x + base.point.x, add_y + base.point.y, image[base.muki][gr_counter / 15], true);
}

