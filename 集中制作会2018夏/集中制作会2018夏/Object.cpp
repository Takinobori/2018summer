#include "Object.h"

Object::Object(tPoint point,tPoint hit_point, bool hit_flag, bool story_flag, tSize size, tSize gr_size, std::string str_pass)
{
	this->point				= point;
	this->hit_point			= hit_point;
	this->size				= size;
	this->gr_size			= gr_size;
	this->hit_flag			= hit_flag;
	this->story_flag		= story_flag;
	this->break_flag		= false;
	this->Obj_Change_Flag	= false;
	this->Change_Object[0]	= 0;
	this->Change_Object[1]	= 0;
	if (story_flag)
	{
		str = str_pass;
	}
}
Object::~Object()
{

}
bool Object::Active(tPlayer * player)
{
	if (Keyboard::Get(KEY_INPUT_RETURN) == 1)
	{
		int add_x = (int)( 48 * cos(PI*((double)player->base.muki + 1.0) / 2.0));
		int add_y = (int)(-64 * sin(PI*((double)player->base.muki + 1.0) / 2.0));
		tPoint p_point = tPoint::Get_Center(player->hit_point, player->base.gr_size);
		p_point.x += add_x;
		p_point.y += add_y;
		int wide  =  player->base.muki      % 2 * 16 + 24;
		int hight = (player->base.muki + 1) % 2 * 16 + 24;
		if (Hit_Flag(p_point, tSize::Get_Size(wide, hight)))
		{
			return true;
		}
	}
	return false;
}
bool Object::Hit_Flag(tPoint point, tSize size)
{
	tPoint center_point = tPoint::Get_Center(this->hit_point, this->gr_size);

	if (hit_flag &&
		abs(center_point.x - point.x) <= (this->size.w + size.w) / 2 &&
		abs(center_point.y - point.y) <= (this->size.h + size.h) / 2)
	{
		return true;
	}
	else
	{
		return false;
	}
}