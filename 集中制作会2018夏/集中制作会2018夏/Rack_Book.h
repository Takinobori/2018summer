#pragma once
#include "Object.h"
class Rack_Book :public Object
{
public:
	Rack_Book(tPoint point, int Text_num);
	~Rack_Book();
	void Update();
	void Draw(int Add_x, int Add_y);
	bool Active(tPlayer* player);
	void Effect();
};

