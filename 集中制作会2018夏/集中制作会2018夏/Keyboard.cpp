#include "Keyboard.h"

int Keyboard::key[256];
char Keyboard::tmpkey[256];

bool Keyboard::Update()
{
	GetHitKeyStateAll(tmpkey);
	if (CheckHitKey(KEY_INPUT_ESCAPE) == 1)
	{
		return true;
	}

	for (int i = 0; i < 256; i++)
	{
		if (tmpkey[i] != 0)
		{
			key[i]++;
		}
		else if (key[i] > 0)
		{
			key[i] = -1;
		}
		else
		{
			key[i] = 0;
		}
	}
	return false;
}
int Keyboard::Get(int keycode)
{
	return key[keycode];
}
