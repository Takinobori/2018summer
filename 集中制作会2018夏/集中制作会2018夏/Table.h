#pragma once
#include "Object.h"
class Table :
	public Object
{
private:
	int image;

public:
	Table(tPoint point);
	~Table();
	void Update();
	void Draw(int Add_x, int Add_y);
	bool Active(tPlayer* player);
	void Effect();
};

