#pragma once
#include "Base_Scene.h"

struct Scene_Result
{
	void Initialize();	// 初期化、画像の読み込みとか
	void Delete();		// 終了処理、画像のメモリ開放とか
	void Update();		// 更新、挙動
	void Draw();		// 描画
};