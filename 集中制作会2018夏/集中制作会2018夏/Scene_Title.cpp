#include "Scene_Title.h"

void Scene_Title::Initialize() {}
void Scene_Title::Delete() {}
void Scene_Title::Update()
{
	// シーンの移動処理(デバック用)
	{
		if (Keyboard::Get(KEY_INPUT_SPACE) == 1)
		{
			Base_Scene::next_scene = eScene_Game;
		}
	}
}
void Scene_Title::Draw()
{
	// デバック用文章
	{
		DrawFormatString(64, 64, 0xffffff, "タイトル画面です。");
		DrawFormatString(64, 64 * 2, 0xffffff, "スペースキーでゲーム画面に移行します。");
	}
}