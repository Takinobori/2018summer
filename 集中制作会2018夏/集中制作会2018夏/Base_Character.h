#pragma once
#include "Point.h"
#include "Size.h"
#include "Muki.h"

struct Base_Character
{
	tPoint  point;
	tPoint  back_point;
	tSize   size;
	tSize   gr_size;
	Muki    muki;

	static Base_Character Get_Base_Character(tPoint p, tSize s, tSize gr_s, Muki muki);
	void Set_Back();
	void Go_Back();
};