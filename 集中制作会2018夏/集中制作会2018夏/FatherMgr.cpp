#include "FatherMgr.h"

void FatherMgr::Initialize(tPoint p, Muki muki)
{
	// 初期化、今は適当な値を入れている
	father.Initialize(p, muki);
}
void FatherMgr::Delete()
{
	// 画像の消去
	father.Delete();
}
void FatherMgr::Update(Map * map, tPlayer* player)
{
	// Updateの中身意外に書くものがあれば書いてもよい

	// 基本挙動
	father.Update(player->base.point);

	////////////////////////////
	// ここから下に壁にぶつかったら〜とかプレイヤーに当たったら〜とか書く場所

}
void FatherMgr::Draw(Map * map)
{
	// 描画、マップに合わせて動かすからあとでちょっと弄る
	father.Draw(map->x, map->y);
}
