#pragma once
#include "math.h"
#include "Size.h"

struct tPoint
{
	int x;
	int y;
	static	tPoint Get_Point(int x, int y);
	static	tPoint Get_Center(tPoint point, tSize gr_size);
			double length(tPoint another);
			double length_H(tPoint another);
			double length_W(tPoint another);
};