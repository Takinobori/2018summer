#include "SceneMgr.h"

void SceneMgr::Initialize()
{
	// 最初のシーンは必ずタイトルなのでタイトルの初期化をする
	title.Initialize();
	Base_Scene::Black_Counter = 255;
}
void SceneMgr::Delete()
{
	// 『今のシーン』を終了する
	switch (Base_Scene::now_scene)
	{
	case eScene_Title:
		title.Delete();
		break;

	case eScene_Game:
		game.Delete();
		break;

	case eScene_Result:
		result.Delete();
		break;

	default:
		break;
	}
}
void SceneMgr::Update()
{
	// もしシーン移動のタイミングなら
	if (Base_Scene::Check_Scene())
	{
		// シーンを変える
		Change_Scene();
	}
	else
	{
		// 『今のシーン』に合わせて更新する
 		switch (Base_Scene::now_scene)
		{
		case eScene_Title:
			title.Update();
			break;

		case eScene_Game:
			game.Update();
			break;

		case eScene_Result:
			result.Update();
			break;

		default:
			break;
		}
	}
	if (Base_Scene::Black_Counter != 255)
	{
		Base_Scene::Black_DOWN(5);
	}
}
void SceneMgr::Draw()
{
	if (Base_Scene::Black_Counter != 255)
	{
		SetDrawBlendMode(DX_BLENDMODE_ALPHA, Base_Scene::Black_Counter);
	}
	// 『今のシーン』に合わせて描画する
	switch (Base_Scene::now_scene)
	{
	case eScene_Title:
		title.Draw();
		break;

	case eScene_Game:
		game.Draw();
		break;

	case eScene_Result:
		result.Draw();
		break;

	default:
		break;
	}
	if (Base_Scene::Black_Counter != 255)
	{
		SetDrawBlendMode(DX_BLENDMODE_ALPHA, 255);
	}
}
void SceneMgr::Change_Scene()
{
	Base_Scene::Black_UP(10);

	if (Base_Scene::Black_Counter == 0)
	{
		// 『今のシーン』を終了する
		switch (Base_Scene::now_scene)
		{
		case eScene_Title:
			title.Delete();
			break;

		case eScene_Game:
			game.Delete();
			break;

		case eScene_Result:
			result.Delete();
			break;

		default:
			break;
		}
		// そのあとに『次のシーン』を初期化する
		switch (Base_Scene::next_scene)
		{
		case eScene_Title:
			title.Initialize();
			break;

		case eScene_Game:
			game.Initialize();
			break;

		case eScene_Result:
			result.Initialize();
			break;

		default:
			break;
		}

		// 『今のシーン』を『次のシーン』に切り替える
		Base_Scene::now_scene = Base_Scene::next_scene;
	}
}