#pragma once
#include "Map.h"
#include "Story.h"
#include "Base_Scene.h"

struct tStage
{
	int         now_stage;
	static bool clear_flag;
	Map*        map;
	MapMode     stage_mode[5];
	tStory*		story;
	tPlayer*	player;

	void Initialize(tStory* _story,tPlayer* _player);
	void Delete();
	bool Go_NextStage();
};