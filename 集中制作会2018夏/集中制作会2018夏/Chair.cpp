#include "Chair.h"



Chair::Chair(tPoint point, Muki _muki) :Object(point, tPoint::Get_Point(point.x, point.y + 32), true, false, tSize::Get_Size(24, 50), tSize::Get_Size(64, 64))
{
	muki = _muki;
	std::string pass = "image/Object/Chair/" + std::to_string(muki) + ".png";
	image = LoadGraph(pass.c_str());
}
Chair::~Chair()
{
	DeleteGraph(image);
}
void Chair::Update()
{
}
void Chair::Draw(int Add_x,int Add_y)
{
	DrawGraph(point.x + Add_x, point.y + Add_y, image, true);
}
bool Chair::Active(tPlayer* player)
{
	return false;
}
void Chair::Effect()
{
}
