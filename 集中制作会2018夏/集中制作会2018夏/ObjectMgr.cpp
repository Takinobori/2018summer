#include "ObjectMgr.h"

void ObjectMgr::Initialize(int stage_num, tPlayer * _player, tStory * _story)
{
	Make_Obj(stage_num);
	player = _player;
	story = _story;
}
void ObjectMgr::Delete()
{
	for (int i = (signed)obj.size() - 1; i > 0; i--)
	{
		delete obj[i];
		obj.pop_back();
	}
}
void ObjectMgr::Make_Obj(int stage_num)
{
	if (!obj.empty())
	{
		for (int i = 0, n = (signed)obj.size(); i < n; i++)
		{
			delete obj[i];
		}
		obj.resize(0);
	}
	std::string pass = "Map/Object/" + std::to_string(stage_num) + ".csv";
	Text_Data data = tText::Get_Text(pass);
	for (int y = 0, n = (signed)data.size(); y < n; y++)
	{
		for (int x = 0, o = (signed)data[y].size(); x < o; x++)
		{
			switch (stoi(data[y][x])/10)
			{
			case 0:
				break;
			case 1:
				obj.emplace_back(new Chair(tPoint::Get_Point(x*PANEL, y*PANEL), tMuki::Get_Muki(stoi(data[y][x]) % 10)));
				break;
			case 2:
				obj.emplace_back(new Table (tPoint::Get_Point(x*PANEL, y*PANEL)));
				break;
			case 3:
				obj.emplace_back(new Rack_Book(tPoint::Get_Point(x*PANEL, y*PANEL), tMuki::Get_Muki(stoi(data[y][x]) % 10)));
				break;
			case 4:
				break;

			case 5:
				break;

			case 6:
				break;

			default:
				break;
			}
		}
	}
}
void ObjectMgr::Update()
{
	if (!obj.empty())
	{
		for (int i = 0, n = (signed)obj.size(); i < n; i++)
		{
			HitToPlayer(i);
			if (obj[i]->Active(player))
			{
				obj[i]->Effect();
				if (obj[i]->story_flag)
				{
					story->Initialize(obj[i]->str, player);
				}
			}
			if (obj[i]->break_flag)
			{

			}
		}
	}
}
void ObjectMgr::Draw(Map* map)
{
	if (!obj.empty())
	{
		for (int i = 0, n = (signed)obj.size(); i < n; i++)
		{
			obj[i]->Draw(map->x, map->y);
		}
	}
}
void ObjectMgr::HitToPlayer(int num)
{
	if (obj[num]->Hit_Flag(tPoint::Get_Center(player->hit_point, player->base.gr_size), player->base.size))
	{
		player->base.Go_Back();
	}
}