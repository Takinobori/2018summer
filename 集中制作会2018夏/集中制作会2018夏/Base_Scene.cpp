#include "Base_Scene.h"

eScene Base_Scene::now_scene;
eScene Base_Scene::next_scene;
int Base_Scene::Black_Counter;

void Base_Scene::Black_UP(int num)
{
	Black_Counter -= num;
	if (Black_Counter <= 0)
	{
		Black_Counter = 0;
	}
}

void Base_Scene::Black_DOWN(int num)
{
	Black_Counter += num;
	if (Black_Counter >= 255)
	{
		Black_Counter = 255;
	}
}

bool Base_Scene::Check_Scene()
{
	// 『今のシーン』と『次のシーン』が違うならばtrue、そうでないならばfalseを返す
	// 『今のシーン』と『次のシーン』が違うならばシーンを切り替える
	return now_scene != next_scene;
}