#include "Stage.h"

bool tStage::clear_flag;

void tStage::Initialize(tStory* _story, tPlayer* _player)
{
	story = _story;
	player = _player;
	now_stage = 0;
	tStage::clear_flag = false;
	map = new Map(eMap_Castle, 0, player);
	std::string pass = "Text/Story/" + std::to_string(now_stage) + ".txt";
	story->Initialize(pass.c_str(), player);
	stage_mode[0] = eMap_Castle;
	stage_mode[1] = eMap_Castle_D;
	stage_mode[2] = eMap_Castle_D;
	stage_mode[3] = eMap_Castle_D;
	stage_mode[4] = eMap_Real;
}
void tStage::Delete()
{
	delete map;
	// story.Delete();
}
bool tStage::Go_NextStage()
{
	if (now_stage == 2)
	{
		Base_Scene::next_scene	= eScene_Result;
		tStory::Flag			= false;
		return false;
	}

	Base_Scene::Black_UP(20);
	if (Base_Scene::Black_Counter == 0)
	{
		tStage::clear_flag = false;
		now_stage++;
		std::string pass = "Text/Story/" + std::to_string(now_stage) + ".txt";
		map->Reset(stage_mode[now_stage], now_stage, player);
		story->Initialize(pass.c_str(), player);
		return true;
	}
	else
	{
		return false;
	}
}
