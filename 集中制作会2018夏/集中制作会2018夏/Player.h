#pragma once
#include "Base_Character.h"
#include "Keyboard.h"
#include <array>
#include <string>

struct tPlayer
{
	Base_Character	base;
	tPoint			hit_point;
	int				gr_counter;
	bool			walk_flag;
	std::array<std::array<int, 3>, 4> image;
	void Initialize(tPoint p, Muki muki);
	void Delete();
	void Update();
	void Draw(int add_x, int add_y);
};