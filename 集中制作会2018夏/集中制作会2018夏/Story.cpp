#include "Story.h"

bool tStory::Flag;
bool tStory::CLEAR_FLAG;

void tStory::Initialize(std::string pass, tPlayer* _player)
{
	tStory::Flag = true;
	tStory::CLEAR_FLAG = false;
	name_flag = false;
	text_x = 0;
	text_y = -1;
	walk_spd_x = 0;
	walk_spd_y = 0;
	walk_count = 0;
	player = _player;
	text = tText::Get_Text(pass);
	Go_Next();
}
void tStory::Update()
{
	switch (mode)
	{
	case eStory_Talk:
		Talk();
		break;
	case eStory_Walk:
		Walk();
		break;
	case eStory_Talk_Walk:
		Talk_Walk();
		break;

	default:
		break;
	}
}
void tStory::Draw()
{
	switch (mode)
	{
	case eStory_Talk:
		DrawGraph(0, 0, image_waku, true);
		if (name_flag)
		{
			if (text[text_y][1] == "主人公")
			{
				DrawGraph(24, 768, image_character[0], true);
			}
			else if (text[text_y][1] == "姫")
			{
				DrawGraph(24, 768, image_character[1], true);
			}
			else
			{
				DrawGraph(24, 768, image_character[2], true);
			}

			DrawFormatString(64, 800, 0xffffff, "%s", text[text_y][1].c_str());
			for (int i = 0, n = (signed)draw_str.size(); i < n; i++)
			{
				DrawFormatString(500, 850 + i * 50, 0xffffff, "%s", draw_str[i].c_str());
			}
		}
		else
		{
			for (int i = 0, n = (signed)draw_str.size(); i < n; i++)
			{
				DrawFormatString(500, 850 + i * 50, 0xffffff, "%s", draw_str[i].c_str());
			}
		}
		break;
	case eStory_Walk:
		break;
	case eStory_Talk_Walk:
		break;

	default:
		break;
	}
}
void tStory::Delete()
{
	DeleteGraph(image_waku);
	for (int i = 0, n = (signed)image_character.size(); i < n; i++)
	{
		DeleteGraph(image_character[i]);
	}
}
void tStory::Go_Next()
{
	text_y++;
	if (text[text_y][0] == "CLEAR")
	{
		tStory::CLEAR_FLAG = true;
	}
	else if (text[text_y][0] == "END")
	{
		tStory::Flag = false;
	}
	else if (text[text_y][0] == "Talk")
	{
		if (text[text_y][1] == "no_name")
		{
			name_flag = false;
		}
		else
		{
			name_flag = true;
		}
		mode = eStory_Talk;
		draw_str.resize(0);
		for (int i = 2, n = (signed)text[text_y].size(); i < n; i++)
		{
			draw_str.push_back(text[text_y][i]);
		}
	}
	else if (text[text_y][0] == "Walk")
	{
		// 歩く、状態にする
		mode = eStory_Walk;
		// 歩くフレーム数の確認
		walk_count = stoi(text[text_y][4]);
		// 歩く方向、速度調整準備
		walk_spd_x = stoi(text[text_y][5]);
		walk_spd_y = stoi(text[text_y][5]);
		// 歩く方向、速度を確認する(向く方向とは別の可能性あり)
		if (text[text_y][2] == "UP")
		{
			walk_spd_x = 0;
			walk_spd_y = -walk_spd_y;
		}
		else if (text[text_y][2] == "LEFT")
		{
			walk_spd_x = -walk_spd_x;
			walk_spd_y = 0;
		}
		else if (text[text_y][2] == "DOWN")
		{
			walk_spd_x = 0;
		}
		else if (text[text_y][2] == "RIGHT")
		{
			walk_spd_y = 0;
		}
		// 向きを確認する
		if (text[text_y][3] == "UP")
		{
			player->base.muki = UP;
		}
		else if (text[text_y][3] == "LEFT")
		{
			player->base.muki = LEFT;
		}
		else if (text[text_y][3] == "DOWN")
		{
			player->base.muki = DOWN;
		}
		else if (text[text_y][3] == "RIGHT")
		{
			player->base.muki = RIGHT;
		}
	}
	else if (text[text_y][0] == "Talk_Walk")
	{
		if (text[text_y][1] == "no_name")
		{
			name_flag = false;
		}
		else
		{
			name_flag = true;
		}
		mode = eStory_Talk_Walk;
	}
}
void tStory::Talk()
{
	if (Keyboard::Get(KEY_INPUT_RETURN) == 1)
	{
		Go_Next();
	}

}
void tStory::Walk()
{
	walk_count--;
	if (text[text_y][1] == "主人公")
	{
		player->base.point.x += walk_spd_x;
		player->base.point.y += walk_spd_y;
		player->gr_counter = (player->gr_counter + 1) % 30;
	}
	if (walk_count == 0)
	{
		Go_Next();
	}
}
void tStory::Talk_Walk()
{
}