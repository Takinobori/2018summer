#pragma once
#include "Base_Character.h"
// Dxlibがキーボードに入ってたから呼びました　キーボードはたぶん使わん
#include "Keyboard.h"

struct tFather
{
	Base_Character base;								// ｘ、ｙ座標と横幅と縦幅、向きが格納された構造体
	void Initialize(tPoint p, Muki muki);				// 変数の初期化、画像や音の読み込みを行うこと
	void Delete();										// 画像、音楽の削除を行うところ、まだ何も必要ないかも
	void Update(tPoint player);							// 基本挙動、仮引数がプレイヤー(追いかけるべき)の座標
	void Draw(int add_x, int add_y);	   				// 描画処理
};