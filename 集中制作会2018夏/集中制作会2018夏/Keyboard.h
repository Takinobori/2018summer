#pragma once
#include <DxLib.h>
#define PANEL 64
#define PI 3.14

struct Keyboard
{
	static int  key[256];
	static char tmpkey[256];
	static bool Update();
	static int Get(int keycode);
};

