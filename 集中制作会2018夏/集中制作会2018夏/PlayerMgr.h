#pragma once
#include "Player.h"
#include "Map.h"

struct PlayerMgr
{
	tPlayer player;
	void Initialize(tPoint p, Muki muki);
	void Delete();
	void Update(Map* map);
	void Draw(Map* map);
};