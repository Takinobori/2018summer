#include "SceneMgr.h"

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	// 画面サイズ
	SetGraphMode(1920, 1080, 32);

	// 全画面に引き延ばす
	ChangeWindowMode(true);

	// DxLibの初期化、失敗したら終了
	if (DxLib_Init() == -1)
	{
		return -1;
	}
	// 裏画面のセット
	SetDrawScreen(DX_SCREEN_BACK);

	// シーンマネージャーの作成、初期化
	SceneMgr scenemgr;
	scenemgr.Initialize();

	//     裏と表の画面を入れ替える  ウィンドウズメッセージを処理     画面に描画していたものを消す
	while (ScreenFlip() == 0 && ProcessMessage() == 0 && ClearDrawScreen() == 0)
	{
		// 何のシーンであってもESCキーで終了
		if (Keyboard::Update())
		{
			break;
		}
		scenemgr.Update();
		scenemgr.Draw();
	}

	scenemgr.Delete();
	// 画像、音楽のメモリを開放する
	InitSoundMem();
	InitGraph();
	// Dxライブラリの終了処理
	DxLib_End();
	return 0;
}