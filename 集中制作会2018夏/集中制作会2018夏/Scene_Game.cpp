#include "Scene_Game.h"

void Scene_Game::Initialize()
{
	tStory::CLEAR_FLAG = false;
	player.Initialize(tPoint::Get_Point(128, 128), UP);
	stage.Initialize(&story, &player.player);
	father.Initialize(tPoint::Get_Point(192, 192), UP);
	object.Initialize(stage.now_stage, &player.player, &story);
	story.image_waku = LoadGraph("image/枠.png");
	for (int i = 0, n = (signed)story.image_character.size(); i < n; i++)
	{
		std::string pass = "image/Character/Talk/" + std::to_string(i) + ".png";
		story.image_character[i] = LoadGraph(pass.c_str());
	}

 
	int a = 0;
}
void Scene_Game::Delete()
{
	player.Delete();
	father.Delete();
	object.Delete();
	stage.Delete();
	story.Delete();
}
void Scene_Game::Update()
{
	// ステージ移動処理
	if (tStory::CLEAR_FLAG)
	{
		tStage::clear_flag = true;
	}
	if (tStage::clear_flag)
	{
		if (stage.Go_NextStage())
		{
			object.Make_Obj(stage.now_stage);
		}
	}
	// シーンの移動処理(デバック用)
	else
	{
		if (Keyboard::Get(KEY_INPUT_SPACE) == 1)
		{
			Base_Scene::next_scene = eScene_Result;
		}
		if (Keyboard::Get(KEY_INPUT_BACK) == 1)
		{
			Base_Scene::next_scene = eScene_Title;
		}
		if (tStory::Flag)
		{
			story.Update();
		}
		else
		{
			player.Update(stage.map);
			object.Update();
			if (Keyboard::Get(KEY_INPUT_N) == 1)
			{
				tStage::clear_flag = true;
			}
		}
	}
}
void Scene_Game::Draw()
{
	stage.map->Draw();
	object.Draw(stage.map);
	player.Draw(stage.map);
	if (tStory::Flag)
	{
		story.Draw();
	}
	// デバック用文章
	{
		DrawFormatString(64, 64, 0xffffff, "ゲーム画面です。");
		DrawFormatString(64, 64 * 2, 0xffffff, "バックスペースでタイトル画面に戻ります。");
		DrawFormatString(64, 64 * 3, 0xffffff, "スペースキーでリザルト画面に移行します。");
		DrawFormatString(64, 64 * 6, 0xff5050, "十字キーで対応の方向へ移動。");
	}
}