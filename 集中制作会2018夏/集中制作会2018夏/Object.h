#pragma once
#include "Keyboard.h"
#include "Text.h"
#include <string>
#include "Point.h"
#include "Size.h"
#include "Player.h"

enum Object_Type
{
	eObj_Dead, eObj_Text,
};
class Object
{
private:

public:
	std::string			str;
	tSize				size;
	tSize				gr_size;
	tPoint				point;
	tPoint				hit_point;
	bool				hit_flag;
	bool				story_flag;
	bool				Obj_Change_Flag;
	int					image;
	std::array<int, 2>	Change_Object;
	Object(tPoint point,tPoint hit_point, bool hit_flag, bool story_flag, tSize size = tSize::Get_Size(0, 0), tSize gr_size = tSize::Get_Size(0, 0), std::string str_pass = "");
	~Object();
	virtual void Update()										= 0;
	virtual void Draw(int Add_x, int Add_y)						= 0;
	virtual void Effect()										= 0;		
			bool Active(tPlayer* player);
			bool Hit_Flag(tPoint center_point, tSize size);
			bool break_flag;
};