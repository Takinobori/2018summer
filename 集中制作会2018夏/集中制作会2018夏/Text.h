#pragma once
#include <DxLib.h>
#include <vector>
#include <array>
#include <string>
#include <fstream>
#include <sstream>
#define Text_Data std::vector<std::vector<std::string>>

struct tText
{
	static Text_Data     data;
	static std::ifstream ifs;

	static Text_Data Get_Text(std::string pass);
};