#include "Table.h"

Table::Table(tPoint point) :Object(point, point, true, false, tSize::Get_Size(180, 333), tSize::Get_Size(256, 384))
{
	image = LoadGraph("image/Object/Table/0.png");
}
Table::~Table()
{
	DeleteGraph(image);
}
void Table::Update()
{
}
void Table::Draw(int Add_x, int Add_y)
{
	DrawGraph(point.x + Add_x, point.y + Add_y, image, true);
}
bool Table::Active(tPlayer* player)
{
	return false;
}
void Table::Effect()
{
}
