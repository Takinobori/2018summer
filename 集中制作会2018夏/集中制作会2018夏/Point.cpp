#include "Point.h"

tPoint tPoint::Get_Point(int x, int y)
{
	tPoint point;
	point.x = x;
	point.y = y;
	return point;
}
tPoint tPoint::Get_Center(tPoint point, tSize gr_size)
{
	tPoint center_point;

	center_point.x = point.x + (gr_size.w / 2);
	center_point.y = point.y + (gr_size.h / 2);

	return center_point;
}
double tPoint::length(tPoint p)
{
	double len;
	len = hypot(x - p.x, y - p.y);
	return len;
}
double tPoint::length_H(tPoint p)
{
	double len_h;
	len_h = hypot(0, y - p.y);
	return len_h;
}
double tPoint::length_W(tPoint p)
{
	double len_w;
	len_w = hypot(x - p.x, 0);
	return len_w;
}