#pragma once
#include "Text.h"
#include "Keyboard.h"
#include "Player.h"
#include "Father.h"

enum eStory_Mode
{
	eStory_Talk, eStory_Walk, eStory_Talk_Walk,
};
struct tStory
{
	static	bool						Flag;
	static	bool						CLEAR_FLAG;
			Text_Data					text;
			eStory_Mode					mode;
			tPlayer*					player;
			int							text_x;
			int							text_y;
			int							walk_spd_x;
			int							walk_spd_y;
			int							image_waku;
			int							walk_count;
			bool						name_flag;
			std::array<int, 3>			image_character;
			std::vector<std::string>	draw_str;

	void Initialize(std::string pass,tPlayer* _player);
	void Update();
	void Draw();
	void Delete();

	void Go_Next();
	bool draw_text_flag;
	void Talk();
	void Walk();
	void Talk_Walk();
};
