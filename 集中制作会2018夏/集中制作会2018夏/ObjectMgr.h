#pragma once
#include "Keyboard.h"
#include "Map.h"
#include "Player.h"
#include "Story.h"
// オブジェクト類
#include "Chair.h"
#include "Table.h"
#include "Rack_Book.h"


struct ObjectMgr
{
	std::vector<Object*>	obj;
	tPlayer*				player;
	tStory*					story;
	void Initialize(int stage_num, tPlayer* _player,tStory* _story);
	void Delete();
	void Make_Obj(int stage_num);
	void Update();
	void Draw(Map* map);
	void HitToPlayer(int num);
//	void ReMake(int num,)
};
