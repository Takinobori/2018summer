#include "PlayerMgr.h"

void PlayerMgr::Initialize(tPoint p, Muki muki)
{
	player.Initialize(p, muki);
}
void PlayerMgr::Delete()
{
	player.Delete();
}
void PlayerMgr::Update(Map* map)
{
	player.Update();
	int x, y;
	for (int i = 0; i < 4; i++)
	{
		x = (int)(tPoint::Get_Center(player.hit_point, player.base.gr_size).x + (player.base.size.w / 2)*sqrt(2)*sin(PI*(i + 0.5) / 2));
		y = (int)(tPoint::Get_Center(player.hit_point, player.base.gr_size).y + (player.base.size.h / 2)*sqrt(2)*cos(PI*(i + 0.5) / 2));

		if (map->data[y / PANEL][x / PANEL] != 1)
		{
			player.base.Go_Back();
		}
	}
}
void PlayerMgr::Draw(Map* map)
{
	player.Draw(map->x, map->y);
}