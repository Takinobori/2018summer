#include "Father.h"

void tFather::Initialize(tPoint p, Muki muki)
{
	base = Base_Character::Get_Base_Character(p, tSize::Get_Size(64, 64), tSize::Get_Size(64, 64), muki);
}
void tFather::Delete()
{
}
void tFather::Update(tPoint player)
{
	if (player.y > base.point.y) // 上に追う
	{
		base.point.y -= 3;
		base.muki = UP;
	}
	if (player.x > base.point.x) // 左に追う
	{
		base.point.x -= 3;
		base.muki = LEFT;
	}
	if (player.y < base.point.y) // 下に追う
	{
		base.point.y += 3;
		base.muki = DOWN;
	}
	if (player.x < base.point.x) // 右に追う
	{
		base.point.x = 3;
		base.muki = RIGHT;
	}
}
void tFather::Draw(int add_x, int add_y)
{
	DrawBox(add_x + base.point.x, add_y + base.point.y, add_x + base.point.x + base.size.w, add_y + base.point.y + base.size.h, 0xFF0000, true);
}